![alt text](https://i.imgur.com/mi6wEKM.png)

# String Length Checker

This is a simple command line interface program. It can determine the longest or shortest word(s) in a sentence.

For input, the program requires a sentence (String), followed by the user's choice of determining either the longest or shortest word in the sentence.
As output, the user's choice of longest or shortest word(s) is displayed, along with the number of characters.

The project was developed with Java 11 and it uses Maven 3.8.0 for dependency management and automation.
i
## Assumptions
1. User input must be sanitized  such that non-alphanumeric characters, including hyphens, are ignored
2. Compound words in hyphenated form are considered single words (e.g. part-time is considered one word)
3. Commas are replaced with spaces

## Project Composition and Dependencies

**Project SDK:** Java version 11.0.7

**Language Level Features:** Java 8: Lambdas used for sorting arrays

**Libraries:** 
- java.util.Scanner
[https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Scanner.html](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Scanner.html)
- java.util.Arrays
 https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Arrays.html
 
 **Dependencies**
- JUnit 5 (Jupiter API & Jupiter Engine)
https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api/5.7.0-M1
https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-engine/5.7.0-M1

 **Maven Plugins**
 - Maven Compiler Plugin 3.8.0
 https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-compiler-plugin/3.8.0
 - Maven Surefire Plugin 2.22.0
https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-surefire-plugin/2.22.0
 - Maven Failsafe Plugin 2.22.0
https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-failsafe-plugin/2.22.0

## Running the Application

1. Download the project to a local directory
2. Open a CLI tool such as Terminal or Command Prompt and navigate to the project directory
3. Navigate further to `stringlengthchecker/src/main/java`
4. Compile the application by typing `javac StringChecker.java`
5. Type `java StringChecker` to run the application and interact with it.

## Building and Executing the Tests

>System requirement: This application uses Maven. In order to run it, you will need to have Maven installed on your system
1. Download the project to a local directory
2. Open a CLI tool such as Terminal or Command Prompt and navigate to the root folder where the `pom.xml` exists
3. Run the following command: `mvn clean test`, to see the test results. 

## Notes
**Test input:**

`fourWords` = "I am @red bear!."

`threeWords` = "I am @red-bear!."

`emptyString` = ""

`whitespace` = " "

`period` = "."

`number` = "012"

`numbers` = "3456 789"

`specialChars` = "!@#$%^&*()_+-"

`withDashes` = "Welcome to the future of tele-medicine"

`twoTwelveLetterWords` = "Welcome to the future of tele-medicine telemedicine"
