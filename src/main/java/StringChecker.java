import java.util.Arrays;
import java.util.Scanner;

public class StringChecker {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Type a sentence to find the longest or shortest word(s).");
        String userSentence = scanner.nextLine();

        if (userSentence.trim().length() <= 0) {
            System.out.println("Sorry, that was an invalid sentence.");
        } else {
            System.out.println("Enter 1 for the longest word(s), Enter 2 for the shortest word(s)");
            String userSelection = scanner.nextLine();

            switch (userSelection) {

                case "1":
                    System.out.println(longestWord(userSentence));
                    break;

                case "2":
                    System.out.println(shortestWord(userSentence));
                    break;

                default:
                    System.out.println("Sorry, that was an invalid choice.");
                    break;
            }
        }
    }

    public static String longestWord(String input) {

        String sentence = sanitizeInput(input);
        String[] arrayOfWords = convertSentenceToArrayOfWords(sentence);
        Arrays.sort(arrayOfWords, (a, b) -> b.length() - a.length());
        return findTopWord(arrayOfWords);
    }

    public static String shortestWord(String input) {

        String sentence = sanitizeInput(input);
        String[] arrayOfWords = convertSentenceToArrayOfWords(sentence);
        Arrays.sort(arrayOfWords, (a, b) -> a.length() - b.length());
        return findTopWord(arrayOfWords);
    }


    public static String sanitizeInput(String input) {

        return input.trim()
                .replaceAll("[^a-zA-Z0-9\\s]", "")
                .replaceAll("\\-", "")
                .replaceAll(",", " ");
    }

    public static String[] convertSentenceToArrayOfWords(String input) {

        return input.split("\\s+");
    }

    public static String findTopWord(String[] input) {

        String targetWordsInSentence = "";

        for (String word : input) {
            if (word.length() == input[0].length()) {
                targetWordsInSentence = targetWordsInSentence + word + ", ";
            }
        }
        String targetWordsAndCharacterCount = targetWordsInSentence + " " + input[0].length();
        return targetWordsAndCharacterCount;
    }

}
