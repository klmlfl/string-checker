import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringCheckerTest {

    String fourWords = "I am @red bear!.";
    String threeWords = "I am @red-bear!.";
    String emptyString = "";
    String whitespace = " ";
    String period = ".";
    String number = "012";
    String numbers = "3456 789";
    String specialChars = "!@#$%^&*()_+-";
    String withDashes = "Welcome to the future of tele-medicine";
    String twoTwelveLetterWords = "Welcome to the future of tele-medicine telemedicine";


    @Test
    void longestWord() {

        assertEquals("bear,  4", StringChecker.longestWord(fourWords));
        System.out.println("***Longest Word [Test 01 → fourWords]***");
        System.out.println("Test of the input such that the longest word is at the end of a sentence + ensure that non-alphanumeric characters are omitted:");
        System.out.println("[Test 01] Passed: Longest word is bear. @ symbol is omitted from character count.\n");

        assertEquals("redbear,  7", StringChecker.longestWord(threeWords));
        System.out.println("***Longest Word [Test 02 → threeWords]***");
        System.out.println("Test of the input such that the longest word is at the end of a sentence + ensure that hyphens are removed from compound words:");
        System.out.println("[Test 02] Passed: Longest word is redbear. @red-bear converted to redbear.\n");

        assertEquals(",  0", StringChecker.longestWord(emptyString));
        System.out.println("***Longest Word [Test 03 → emptyString]***");
        System.out.println("Test of the input such that it is an empty string:");
        System.out.println("[Test 03] Passed: For an empty string, the longest word is 0\n");

        assertEquals(",  0", StringChecker.longestWord(whitespace));
        System.out.println("***Longest Word [Test 04 → whitespace]***");
        System.out.println("Test of the input such that it is a single whitespace:");
        System.out.println("[Test 04] Passed: For a whitespace, the longest word is 0\n");

        assertEquals(",  0", StringChecker.longestWord(period));
        System.out.println("***Longest Word [Test 05 → period]***");
        System.out.println("Test of the input such that it is a single special character:");
        System.out.println("[Test 05] Passed: For a .(period), the longest word is 0\n");

        assertEquals("012,  3", StringChecker.longestWord(number));
        System.out.println("***Longest Word [Test 06 → number ]***");
        System.out.println("Test of the input such that it is a single word consisting of numbers:");
        System.out.println("[Test 06] Passed: For a single word sentence of 012, the longest word is 012 at 3 characters\n");

        assertEquals("3456,  4", StringChecker.longestWord(numbers));
        System.out.println("***Longest Word [Test 07 → numbers]***");
        System.out.println("Test of the input such that the longest word is at the beginning of the sentence:");
        System.out.println("[Test 07] Passed: For a two word sentence of 3456 789, the longest word is 3456 at 4 characters\n");

        assertEquals(",  0", StringChecker.longestWord(specialChars));
        System.out.println("***Longest Word [Test 08 → specialChars]***");
        System.out.println("Test of the input such that the entire string is composed of special characters:");
        System.out.println("[Test 08] Passed: For a sentence composed of !@#$%^&*()_+-, the longest word is 0\n");

        assertEquals("telemedicine,  12", StringChecker.longestWord(withDashes));
        System.out.println("***Longest Word [Test 09 → withDashes]***");
        System.out.println("Test of the input such that the longest word is composed of a hyphen:");
        System.out.println("[Test 09] Passed: For a sentence containing tele-medicine as the longest word, the longest word is 12\n");

        assertEquals("telemedicine, telemedicine,  12", StringChecker.longestWord(twoTwelveLetterWords));
        System.out.println("***Longest Word [Test 10 → twoTwelveLetterWords]***");
        System.out.println("Test of the input such that the longest word is repeated in the sentence:");
        System.out.println("[Test 10] Passed: For a sentence containing tele-medicine and telemedicine as the longest word, the longest word is telemedicine 12\n");

    }

    @Test
    void shortestWord() {
        assertEquals("I,  1", StringChecker.shortestWord(fourWords));
        System.out.println("***Shortest Word [Test 01 → fourWords]***");
        System.out.println("Test of the input such that the shortest word is at the beginning.");
        System.out.println("[Test 01] Passed: Shortest word is I.\n");

        assertEquals(",  0", StringChecker.shortestWord(emptyString));
        System.out.println("***Shortest Word [Test 02 → emptyString]***");
        System.out.println("Test of the input such that it is an empty string:");
        System.out.println("[Test 03] Passed: For an empty string, the shortest word is 0 characters\n");

        assertEquals(",  0", StringChecker.shortestWord(whitespace));
        System.out.println("***Shortest Word [Test 03 → whitespace]***");
        System.out.println("Test of the input such that it is a single whitespace:");
        System.out.println("[Test 04] Passed: For a whitespace, the shortest word is 0 characters\n");

        assertEquals(",  0", StringChecker.shortestWord(period));
        System.out.println("***Shortest Word [Test 04 → period]***");
        System.out.println("Test of the input such that it is a single special character:");
        System.out.println("[Test 05] Passed: For a .(period), the shortest word is 0 characters\n");

        assertEquals("012,  3", StringChecker.shortestWord(number));
        System.out.println("***Shortest Word [Test 05 → number]***");
        System.out.println("Test of the input such that it is a single word consisting of numbers:");
        System.out.println("[Test 06] Passed: For a single word sentence of 012, the shortest word is 012 at 3 characters\n");

        assertEquals("789,  3", StringChecker.shortestWord(numbers));
        System.out.println("***Shortest Word [Test 06 → numbers]***");
        System.out.println("Test of the input such that the longest word is at the beginning of the sentence:");
        System.out.println("[Test 07] Passed: For a two word sentence of 3456 789, the shortest word is 789 at 3 characters\n");


        assertEquals(",  0", StringChecker.shortestWord(specialChars));
        System.out.println("***Shortest Word [Test 07 → specialChars]***");
        System.out.println("Test of the input such that the entire string is composed of special characters:");
        System.out.println("[Test 08] Passed: For a sentence composed of !@#$%^&*()_+-, the shortest word is 0 characters\n");

        assertEquals("to, of,  2", StringChecker.shortestWord(withDashes));
        System.out.println("***Shortest Word [Test 08 → withDashes]***");
        System.out.println("Test of the input such that the longest word is composed of a hyphen:");
        System.out.println("[Test 09] Passed: For a sentence containing two shortest words, both are displayed with the correct character count of 2\n");
    }
}